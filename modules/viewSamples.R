viewSamplesUI <- function(id) {
	ns <- NS(id)

	tagList(
		shinyIsBusyPanel(),
		fluidRow(
			box(
				title = "Sample information",
				status = "primary",
				solidHeader = T,
				width = 12,
				dataTableOutput(ns("sampleTable"))
			)
		)
	)
}

viewSamples <- function(input, output, session) {
	pathSamples <- file.path(pathInput, "samples.txt")

	dsSamples <- reactive({
		read_tsv(pathSamples, col_types = "cDicccc") %>%
				mutate(Species = paste(Genus, Species, sep = " ")) %>%
				select(-Genus)
	})

	output$sampleTable <- renderDataTable({
		dsSamples()
	})

	dsSamples
}

viewSamplesIcon <- function() {
	icon("leaf")
}
