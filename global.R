requireOrInstall <- function(pkgList) {
	for (pkg in pkgList) {
		if ( ! require(pkg, character.only = T) ) {
			install.packages(pkg, quiet=T)
			require(pkg, character.only = T)
		}
	}
}

requireOrInstall(c(
		"shiny", 
		"shinydashboard", 
		"shinyBS", 
		"stringr", 
		"readr", 
		"reshape2", 
		"tidyr", 
		"dplyr", 
		"ggplot2"
))


# Configuration
tairLinkTempl <- '<a href="http://www.arabidopsis.org/servlets/TairObject?name=%s&type=locus" target="_blank">%s</a>'
poplarGeneLinkTempl <- '<a href="http://bioinformatics.caf.ac.cn/PoplarGene/detail_popGene.php?id=%s" target="_blank">%s</a>'
pathData <- "data"
pathInput <- file.path(pathData, "00_input")
pathCombined <- file.path(pathData, "03_combined")

source("misc.R")

modules <- c(
	"viewSamples",
	"viewAhrd",
	"viewBlastAtha",
	"viewBlastPtri",
	"expression",
	"fancyPlot"
)

for (m in modules){
  source(file.path("modules", paste0(m, ".R")))
}

# I swapped green and vermillion because green is too close to light blue
# orange, sky blue, vermillion, yellow, blue, green, reddish purple, grey, black
defPalette <- c("#E69F00", "#56B4E9", "#F0E442", "#D55E00", "#0072B2", "#009E73", "#CC79A7", "#999999", "#000000")
# This will keep the colours stable until we run out of the base colours
mkSubPalette <- function(n, plt = defPalette) {
	paletteFunc <- colorRampPalette(defPalette)
	pltN <- length(plt)
	if (n < pltN) {
		paletteFunc(pltN)[1:n]
	} else {
		paletteFunc(n)
	}
}

